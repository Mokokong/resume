# Project Name: *Resume*

## Implementation:
	* HTML
	* CSS
	* Javascript
	
## Premise
	* Design a resume site
	

## Requirements for execution:
	* Any browser- preferably Chrome, extensive testing on other browsers not complete
	
## Completion:
	* Display sections :100%
		- Education 100%
		- Experience 100%
		- Projects 100%
		- Skills 100%
		- Volunteer work 100%
		
	* Fill out information sections: 80%
		- Education 100%
		- Experience 100%
		- Projects 80%
		- Skills 90%
		- Volunteer work 100%
	
## Optional completion:
	*