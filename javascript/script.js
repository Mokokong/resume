/*  Section toggle functions
----------------------------------------------------------------------*/

// Section id vars
var divexp = document.querySelector("#exp");
var divski = document.querySelector("#ski");
var dived = document.querySelector("#ed");
var divvol = document.querySelector("#vol");
var divpro = document.querySelector("#pro");
var divme = document.querySelector("#me");


// //Button id vars for addEventListeners
const butexp = document.getElementById("exp_but");
// var butski = document.getElementById("ski_but");
// var buted = document.getElementById("edu_but");
// var butvol = document.getElementById("vol_but");
//
//
// function toggleExperience() {
//     console.log("Toggle activated");
//
//     if (divexp.style.display === "none") {
//       //  console.log("1st branch");
//
//     /*  Div behaviour
//     *    hide all other divs in this section
//     */
//
//         divski.style.display = "none";
//         dived.style.display = "none";
//         divvol.style.display = "none";
//         divpro.style.display = "none";
//         divme.style.display = "none";
//
//      //   unhide this div
//      /*
//      *testing different activation of display value
//      * no new development
//
//     * divexp.setAttribute( "style" ,"display:block") ;
//
//     */
//         divexp.style.display = "block";
//
//     } else {
//       console.log("2nd branch");
//
//       divexp.style.display = "none";
//     //  divdef.style.display = "block";
//
//
//     }
// }


//
//
// function toggleSkills() {
//
//     if (divski.style.display === "none") {
//
//       // hide all other divs in this section
//       divexp.style.display = "none";
//       dived.style.display = "none";
//       divvol.style.display = "none";
//       divpro.style.display = "none";
//       divme.style.display = "none";
//       // unhide this div
//       divski.style.display = "block";
//     } else {
//
//         divski.style.display = "none";
//     }
// }
//
//
// function toggleEducation() {
//     ;
//     if (dived.style.display === "none") {
//
//       // hide all other divs in this section
//
//         divexp.style.display = "none";
//         divski.style.display = "none";
//         divvol.style.display = "none";
//         divpro.style.display = "none";
//         divme.style.display = "none";
//         // unhide this div
//         dived.style.display = "block";
//     } else {
//         dived.style.display = "none";
//     }
// }
//
//
// function toggleVolunteer() {
//
//     if (divvol.style.display === "none") {
//
//       // hide all other divs in this section
//       divexp.style.display = "none";
//       divski.style.display = "none";
//       dived.style.display = "none";
//       divpro.style.display = "none";
//       divme.style.display = "none";
//         // unhide this div
//       divvol.style.display = "block";
//     } else {
//       divvol.style.display = "none";
//     }
// }
//
//
// function toggleProjects(){
//
//     if (divpro.style.display === "none") {
//
//     /*  Div behaviour
//     *    hide all other divs in this section
//     */
//
//         divski.style.display = "none";
//         dived.style.display = "none";
//         divvol.style.display = "none";
//         divexp.style.display = "none";
//         divme.style.display = "none";
//      //   unhide this div
//         divpro.style.display = "block";
//
//
//     } else {
//       divpro.style.display = "none";
//
//     }
// }
//
// function toggleMe() {
//
//     if (divme.style.display === "none") {
//
//     /*  Div behaviour
//     *    hide all other divs in this section
//     */
//         divexp.style.display = "none";
//         divski.style.display = "none";
//         dived.style.display = "none";
//         divvol.style.display = "none";
//         divpro.style.display = "none";
//
//      //   unhide this div
//         divme.style.display = "block";
//
//
//     } else {
//       divme.style.display = "none";
//     //  divdef.style.display = "block";
//
//
//     }
// }


// class removal based display

function toggleExperience() {
    console.log("Toggle activated");
    console.log(divexp.classList);
    if (divexp.classList.contains("hidediv")) {
      //  console.log("1st branch");

    /*  Div behaviour
    *    hide all other divs in this section
    */

        divski.classList.replace ("showdiv","hidediv");
        dived.classList.replace ("showdiv","hidediv");
        divvol.classList.replace ("showdiv","hidediv");
        divpro.classList.replace ("showdiv","hidediv");
        divme.classList.replace ("showdiv","hidediv");

     //   unhide this div
     /*
     *testing different activation of display value
     * no new development

    * divexp.setAttribute( "style" ,"display:block") ;

    */
        divexp.classList.replace ("hidediv","showdiv");

    } else {
      console.log("2nd branch");

      divexp.classList.replace ("showdiv","hidediv");
    //  divdef.style.display = "block";


    }
}




function toggleSkills() {

    if (divski.classList.contains("hidediv")) {

      // hide all other divs in this section
      divexp.classList.replace ("showdiv","hidediv");
      dived.classList.replace ("showdiv","hidediv");
      divvol.classList.replace ("showdiv","hidediv");
      divpro.classList.replace ("showdiv","hidediv");
      divme.classList.replace ("showdiv","hidediv");
      // unhide this div
      divski.classList.replace ("hidediv","showdiv");
    } else {

        divski.classList.replace ("showdiv","hidediv");
    }
}


function toggleEducation() {

    if (dived.classList.contains("hidediv")) {

      // hide all other divs in this section

        divexp.classList.replace ("showdiv","hidediv");
        divski.classList.replace ("showdiv","hidediv");
        divvol.classList.replace ("showdiv","hidediv");
        divpro.classList.replace ("showdiv","hidediv");
        divme.classList.replace ("showdiv","hidediv");
        // unhide this div
        dived.classList.replace ("hidediv","showdiv");
    } else {
        dived.classList.replace ("showdiv","hidediv");
    }
}


function toggleVolunteer() {

    if (divvol.classList.contains("hidediv")) {

      // hide all other divs in this section
      divexp.classList.replace ("showdiv","hidediv");
      divski.classList.replace ("showdiv","hidediv");
      dived.classList.replace ("showdiv","hidediv");
      divpro.classList.replace ("showdiv","hidediv");
      divme.classList.replace ("showdiv","hidediv");
        // unhide this div
      divvol.classList.replace ("hidediv","showdiv");
    } else {
      divvol.classList.replace ("showdiv","hidediv");
    }
}


function toggleProjects(){

    if (divpro.classList.contains("hidediv")) {

    /*  Div behaviour
    *    hide all other divs in this section
    */

        divski.classList.replace ("showdiv","hidediv");
        dived.classList.replace ("showdiv","hidediv");
        divvol.classList.replace ("showdiv","hidediv");
        divexp.classList.replace ("showdiv","hidediv");
        divme.classList.replace ("showdiv","hidediv");
     //   unhide this div
        divpro.classList.replace ("hidediv","showdiv");


    } else {
      divpro.classList.replace ("showdiv","hidediv");

    }
}

function toggleMe() {

    if (divme.classList.contains("hidediv")) {

    /*  Div behaviour
    *    hide all other divs in this section
    */
        divexp.classList.replace ("showdiv","hidediv");
        divski.classList.replace ("showdiv","hidediv");
        dived.classList.replace ("showdiv","hidediv");
        divvol.classList.replace ("showdiv","hidediv");
        divpro.classList.replace ("showdiv","hidediv");

     //   unhide this div
        divme.classList.replace ("hidediv","showdiv");


    } else {
      divme.classList.replace ("showdiv","hidediv");
    //  divdef.style.display = "block";


    }
}


// add event listeners
butexp.addEventListener('click',

function() {
  console.log("Event listener works");
  toggleExperience()
} );
